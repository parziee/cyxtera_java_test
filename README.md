### Run the Application

To run just the API you can simply run the following:

```
./mvnw spring-boot:run
```


### API services call example

- GET: `http://localhost:8090/order?yellow=2&pink=5&blue=6&green=1&white=8`
---


