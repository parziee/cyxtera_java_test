package only;

/**
 * Created by Parzifal on 8/22/18.
 */


public final class Constants  {
    public static final String WHITE = "white";
    public static final String YELLOW = "yellow";
    public static final String PINK = "pink";
    public static final String GREEN = "green";
    public static final String BLUE = "blue";

    public static final int T_SHIRT_PRICE = 60000;
    public static final int FIVE_COLOR_DISCOUNT = 25;//percentage
    public static final int FOUR_COLOR_DISCOUNT = 20;//percentage
    public static final int THREE_COLOR_DISCOUNT = 10;//percentage
    public static final int TWO_COLOR_DISCOUNT = 5;//percentage


}