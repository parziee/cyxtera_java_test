package only;

import java.util.List;

/**
 * Created by Parzifal on 8/22/18.
 */
public class Order {
    private List<Tshirt> tShirtList;
    private int totalPrice;


    public Order(List<Tshirt> tShirtList) {
        this.tShirtList = tShirtList;
        int totalShirts = getTotalShirts(tShirtList);
        int result;

        switch (tShirtList.size()) {
            case 5:
                result = getDeal(Constants.FIVE_COLOR_DISCOUNT, totalShirts);
                break;
            case 4:
                result = getDeal(Constants.FOUR_COLOR_DISCOUNT, totalShirts);
                break;
            case 3:
                result = getDeal(Constants.THREE_COLOR_DISCOUNT, totalShirts);
                break;
            case 2:
                result = getDeal(Constants.TWO_COLOR_DISCOUNT, totalShirts);
                break;
            default:
                result = totalShirts * Constants.T_SHIRT_PRICE;
                break;
        }

        setTotalPrice(result);

    }

    private int getDeal(int discount, int totalShirts) {
        int total = totalShirts * Constants.T_SHIRT_PRICE;
        return total - (total * discount) / 100;
    }

    private int getTotalShirts(List<Tshirt> tShirtList) {
        int totalShirts = 0;
        for (Tshirt t : tShirtList) {
            totalShirts += t.getQuantity();
        }
        return totalShirts;
    }

    public List<Tshirt> gettShirtList() {
        return tShirtList;
    }

    public void settShirtList(List<Tshirt> tShirtList) {
        this.tShirtList = tShirtList;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }
}
