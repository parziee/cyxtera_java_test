package only;

/**
 * Created by Parzifal on 8/22/18.
 */
import java.util.ArrayList;
import java.util.List;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderController {

    @RequestMapping("/order")
    public Order order(
            @RequestParam(value="white", defaultValue= "0") int white_q,
            @RequestParam(value="yellow", defaultValue= "0") int yellow_q,
            @RequestParam(value="pink", defaultValue= "0") int pink_q,
            @RequestParam(value="green", defaultValue= "0") int green_q,
            @RequestParam(value="blue", defaultValue= "0") int blue_q){

        List<Tshirt> tshirtLists = new ArrayList<>();

        if(white_q != 0){
            Tshirt white = new Tshirt(Constants.WHITE, white_q);
            tshirtLists.add(white);
        }
        if(yellow_q != 0){
            Tshirt yellow = new Tshirt(Constants.YELLOW, yellow_q);
            tshirtLists.add(yellow);
        }
        if(pink_q != 0){
            Tshirt pink = new Tshirt(Constants.PINK, pink_q);
            tshirtLists.add(pink);
        }
        if(green_q != 0){
            Tshirt green = new Tshirt(Constants.GREEN, green_q);
            tshirtLists.add(green);
        }
        if(blue_q != 0){
            Tshirt blue = new Tshirt(Constants.BLUE, blue_q);
            tshirtLists.add(blue);
        }

        return new Order(tshirtLists);
    }
}
