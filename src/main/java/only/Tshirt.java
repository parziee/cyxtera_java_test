package only;

/**
 * Created by Parzifal on 8/22/18.
 */
public class Tshirt {
    private String color;
    private int quantity;

    public Tshirt(String color, int quantity) {
        this.color = color;
        this.quantity = quantity;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
